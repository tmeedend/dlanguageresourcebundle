module main;

import std.stdio;

void main(string[] args)
{
	// Prints "Hello World" string in console
	writeln("Unit tests passed");
	
	// Lets the user press <Return> before program returns
	stdin.readln();
}

