module resourcebundle.ResourceBundle;

import std.csv;
import std.stdio;
import std.stream;
import std.string;

import std.c.locale;

import std.windows.registry;

/*
 * Reads a CSV file and creates an associative array of a desired CSV column. 
 * This class can be used to internationalize the code. The locale used in either 
 * the user default locale or the one parametered
 * 
 * CSV example : 
 * key,en_EN,fr_FR,
 * window.close,Close,Fermer,
 * window.new,New,Nouveau,
 * 
 * @see unit tests for usage
 * */

class ResourceBundle

{
	// immutable attributes
	private immutable string _currentLocale;


	// mutable attributes
	private int _numberOfRegisteredLanguages = 0;
	private string[string] _localeValues;

	// struct for CSV
	struct KeyValue {string key; string value; }

	// constructor
	this(immutable string parLocale)
	{
		_currentLocale = parLocale;
	}


	// static functions

	/* instanciate resource bundle from a given CSV file
	CSV file header must have be : key,locale1,locale2, eg key,en_EN,fr_FR,
	CSV content lines must be : my.key,locale1 translation, locale2 translation, eg window.close,Close,Fermer,
	the locale used is user prefered locale or en_EN if no locale is found
	@parFilePath : the CSV file to read
	 */
	static public ResourceBundle getBundle(string parFilePath){
		return getBundle(parFilePath, getUserDefaultLanguage());
	}
	
	/* instanciate resource bundle from a given CSV file
	CSV file header must have be : key,locale1,locale2, eg key,en_EN,fr_FR,
	CSV content lines must be : my.key,locale1 translation, locale2 translation, eg window.close,Close,Fermer,
	@parFilePath : the CSV file to read
	@parLocale : the locale to use. If the locale is not found in the CSV, en_EN will be used
	 */
	static public ResourceBundle getBundle(string parFilePath, string parLocale){


		ResourceBundle locBundle = new ResourceBundle(parLocale);
		BufferedFile locBufferedFile = new BufferedFile(parFilePath);
		
		bool locCurrentLocaleFound = false;
		
		char[] locHeaderLine;
		
		foreach(ulong i, char[] locLine; locBufferedFile) { 
			writefln("processing line %d: %s", i, locLine); 
			
			if(i == 1){ // looking for current locale index
				
				char[][] locLocales = split(locLine, ",");
				writefln("number of locales %d", locLocales.length);
				foreach(char[] locLocale;locLocales){
					writefln("processing locale %s", locLocale);
					if(locLocale == locBundle._currentLocale){
						locCurrentLocaleFound = true;
						locHeaderLine = locLine.dup;
						writefln("System locale %s found in CSV, header Line : %s", parLocale, locHeaderLine);
						break;
					}
				}
				
			} else if(!locCurrentLocaleFound){
				writefln("Cannot find user locale in CSV, exiting ");
				return null;
			} else {
				writefln("locHeaderLine %s", locHeaderLine);
				char[] locCsvToProcess = locHeaderLine ~ '\n' ~ locLine ~ '\n';
				writefln("processing CSV :\n%s", locCsvToProcess);
				auto records = csvReader!KeyValue(locCsvToProcess, ["key",locBundle._currentLocale]);
				foreach(record; records) {
					
					writefln("processing record : %s", record);
					
					writefln("key: %s value: %s", record.key, record.value); 
					locBundle._localeValues[record.key] = record.value.dup;
				}
			} 
		} 
		locBufferedFile.close();
		
		
		return locBundle;
	}

	private static string getUserDefaultLanguage(){
		 try {
			string locRegisterLanguage = Registry.currentUser.getKey("Control Panel\\Desktop\\MuiCached").getValue("MachinePreferredUILanguages").value_MULTI_SZ[0];
			return tr(locRegisterLanguage, "-" ,"_");
		} catch (Exception e){
			return "en_EN";
		}
	}


	// instance functions

	/* get value from given key */
	public string getString(string parKey) {
		return _localeValues[parKey];
	}

	/* get initialized Locale */
	public string getLocale(){
		return this._currentLocale;
	}

	unittest { 
		ResourceBundle bundle = ResourceBundle.getBundle("../../data/tests/test.csv", "fr_FR");
		assert("Nouveau" == bundle.getString("window.new"));
		assert("Fermer" == bundle.getString("window.close"));
		bundle = ResourceBundle.getBundle("../../data/tests/test.csv", "en_EN");
		assert("New" == bundle.getString("window.new"));
		assert("Close" == bundle.getString("window.close"));

		assert(ResourceBundle.getBundle("../../data/tests/test.csv").getLocale() != null);
	}
}

